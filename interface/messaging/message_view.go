package messaging

import (
	"fmt"
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/jroimartin/gocui"
	log "github.com/sirupsen/logrus"
	"gitlab.com/winwizard/viscord/interface/util"
)

type MessageView struct {
	handlerRemoveFuncs []func()
	channelID          string
	mutex              sync.Mutex
	view               *gocui.View
	logger             *log.Entry
	update             func()
}

//TODO figure out whether i should use a single view or multiple also maybe generate a uuid for view
func NewMessageView(logger *log.Entry, update func()) *MessageView {
	mv := &MessageView{
		logger: logger.WithFields(log.Fields{"View": "MessageView"}),
		update: update,
	}
	return mv
}

func (mv *MessageView) Update(session *discordgo.Session, channel *discordgo.Channel, view *gocui.View) {
	var guildName string
	mv.mutex.Lock()
	if guild, err := session.Guild(channel.GuildID); err != nil {
		guildName = channel.GuildID
	} else {
		guildName = guild.Name
	}
	name := guildName + "-" + channel.Name
	//TODO add gaps bet/n views
	view.Editable = false
	view.Highlight = true
	view.Frame = true
	view.Title = name
	view.Wrap = true
	view.Autoscroll = true
	mv.removeHandlers()
	mv.channelID = channel.ID
	mv.view = view
	mv.handlerRemoveFuncs = append(mv.handlerRemoveFuncs, session.AddHandler(mv.messageCreateHandler))
	mv.mutex.Unlock()
}

func (mv *MessageView) removeHandlers() {
	for _, fn := range mv.handlerRemoveFuncs {
		fn()
	}
	mv.handlerRemoveFuncs = []func(){}
}

func (mv *MessageView) messageCreateHandler(s *discordgo.Session, mc *discordgo.MessageCreate) {
	if mc.Message.ChannelID == mv.channelID {
		fmt.Fprintln(mv.view, mv.formatMessage(mc.Message, s.State))
		mv.update()
	}
}

func (mv *MessageView) formatMessage(msg *discordgo.Message, state *discordgo.State) string {
	userColorRGB := state.UserColor(msg.Author.ID, msg.ChannelID)
	return util.RGBString(msg.Author.Username, userColorRGB) + ": " + msg.Content
}
