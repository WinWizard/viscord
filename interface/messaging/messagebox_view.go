package messaging

import (
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/jroimartin/gocui"
	log "github.com/sirupsen/logrus"
)

type MessageboxView struct {
	view               *gocui.View
	session            *discordgo.Session
	channelID          string
	handlerRemoveFuncs []func()
	mutex              sync.Mutex
	logger             *log.Entry
	update             func()
}

func NewMessageboxView(logger *log.Entry, update func()) *MessageboxView {
	mbv := &MessageboxView{
		logger: logger.WithFields(log.Fields{"View": "MessageboxView"}),
		update: update,
	}
	return mbv
}

func (mbv *MessageboxView) Update(session *discordgo.Session, channelID string, view *gocui.View) {
	mbv.mutex.Lock()
	view.Editable = true
	view.Highlight = true
	view.Frame = true
	view.Title = "Message Input"
	view.Wrap = true
	mbv.session = session
	mbv.channelID = channelID
	mbv.view = view
	mbv.removeHandlers()

	mbv.mutex.Unlock()
}
func (mbv *MessageboxView) removeHandlers() {
	for _, fn := range mbv.handlerRemoveFuncs {
		fn()
	}
	mbv.handlerRemoveFuncs = []func(){}
}
