package messaging

import (
	"github.com/bwmarrin/discordgo"
	"github.com/jroimartin/gocui"
	log "github.com/sirupsen/logrus"
)

type MessageWindow struct {
	session    *discordgo.Session
	sidebar    *SidebarView
	messagebox *MessageboxView
	messages   *MessageView
	channel    *discordgo.Channel
	gui        *gocui.Gui
	logger     *log.Entry
}

func NewMessageWindow(session *discordgo.Session, channel *discordgo.Channel, gui *gocui.Gui, logger *log.Entry) *MessageWindow {
	mw := &MessageWindow{
		session: session,
		channel: channel,
		gui:     gui,
		logger:  logger.WithFields(log.Fields{"System": "MessageWindow"}),
	}
	mw.messagebox = NewMessageboxView(logger, mw.update)
	mw.messages = NewMessageView(logger, mw.update)
	mw.sidebar = NewSidebarView(logger, mw.update)
	gui.SetManagerFunc(mw.layout)
	return mw
}

func (mw *MessageWindow) update() {
	mw.gui.Update(mw.layout)
}

func (mw *MessageWindow) SetCurrentChannel(channel *discordgo.Channel) {
	mw.channel = channel
	mw.gui.Update(mw.layout)
}

func (mw *MessageWindow) layout(g *gocui.Gui) error {
	var err error
	var sbv, mbv, mv *gocui.View
	maxX, maxY := g.Size()
	sbpos := []int{0, 0, maxX / 6, maxY - 1}
	mbpos := []int{sbpos[2] + 1, maxY - maxY/6, maxX - 1, maxY - 1}
	mvpos := []int{sbpos[2] + 1, 0, maxX - 1, mbpos[1] - 1}
	if sbv, err = g.SetView("Channels", sbpos[0], sbpos[1], sbpos[2], sbpos[3]); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		//TODO sidebar draw stuffs
	}
	mw.sidebar.Update(sbv, mw.session, mw.channel.ID, mw.channel.ID)

	if mv, err = g.SetView("Messages"+mw.channel.ID, mvpos[0], mvpos[1], mvpos[2], mvpos[3]); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		//TODO sidebar draw stuffs
	}
	mw.messages.Update(mw.session, mw.channel, mv)
	if mbv, err = g.SetView("MessageBox"+mw.channel.ID, mbpos[0], mbpos[1], mbpos[2], mbpos[3]); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		//TODO sidebar draw stuffs
	}
	mw.messagebox.Update(mw.session, mw.channel.ID, mbv)
	return nil
}
