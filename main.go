package main

import (
	"fmt"
	"syscall"

	"github.com/jroimartin/gocui"
	log "github.com/sirupsen/logrus"
	"gitlab.com/winwizard/viscord/interface/menu"
	"gitlab.com/winwizard/viscord/interface/messaging"
	"golang.org/x/crypto/ssh/terminal"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/winwizard/viscord/config"
)

//TODO Use jroimartin/gocui for ui

var gconf *config.GlobalConfig

func main() {
	var session *discordgo.Session
	var err error
	logger := log.WithFields(log.Fields{"App": "viscord"})
	gconf, err = config.InitConfig()
	if err != nil {
		logger.WithError(err).Fatal()
	}

	session, err = getSession(gconf.User.Token)
	if err != nil {
		logger.WithError(err).Fatal()
	}
	m := menu.NewMenu(&menu.MenuConfig{Session: session})
	channel, err := m.GuildMenu()
	if err != nil {
		logger.WithError(err).Fatal()
	}

	gui, err := gocui.NewGui(gocui.Output256)
	defer gui.Close()
	if err != nil {
		logger.WithError(err).Fatal()
	}
	// Set GUI managers and key bindings
	// ...

	session.Open()
	defer session.Close()
	mw := messaging.NewMessageWindow(session, channel, gui, logger)
	_ = mw
	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		logger.WithError(err).Fatal()
	}
	if err := gui.MainLoop(); err != nil && err != gocui.ErrQuit {
		// handle error
		logger.WithError(err).Fatal()
	}

}

func getSession(token string) (*discordgo.Session, error) {
	var session *discordgo.Session
	var err error
	if token == "" {
		return login()
	}
	session, err = discordgo.New(token)
	if err != nil {
		log.WithFields(log.Fields{"Token": token, "Error": err}).Errorf("Login Failed.")
		return login()
	}
	//	session.LogLevel = discordgo.LogDebug
	return session, nil
}

func login() (*discordgo.Session, error) {
	var session *discordgo.Session
	var err error

	for session == nil {
		var username string
		fmt.Println("Login")
		fmt.Printf("Username: ")
		_, err = fmt.Scanf("%s", &username)
		if err != nil {
			return nil, err
		}

		fmt.Printf("Password: ")
		password, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return nil, err
		}
		fmt.Println()

		session, err = discordgo.New(username, string(password))
		if err != nil {
			log.WithFields(log.Fields{"Username": username, "Error": err}).Errorf("Login Failed.")
		}
	}
	gconf.User.Token = session.Token
	gconf.WriteConfig()
	return session, nil
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}
