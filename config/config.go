package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"os/user"
	"path"

	log "github.com/sirupsen/logrus"
)

type GlobalConfig struct {
	User    userConfig    `json:"user"`
	General generalConfig `json:"general,omitempty"`
}
type generalConfig struct {
	LogLevel log.Level `json:"log_level,omitempty"`
}
type userConfig struct {
	Token string `json:"token"`
}

var (
	configPath string
	configFile string
)

func InitConfig() (*GlobalConfig, error) {
	var err error
	var gconf *GlobalConfig

	usr, err := user.Current()
	if err != nil {
		return nil, err
	}

	configPath = path.Join(usr.HomeDir, ".viscord/")
	configFile = "config.json"

	//set up config
	if _, err = os.Stat(path.Join(configPath, configFile)); err != nil {
		gconf = defaultConfig()
		err = gconf.WriteConfig()
		if err != nil {
			return nil, err
		}

	} else {
		configBytes, err := ioutil.ReadFile(path.Join(configPath, configFile))
		if err != nil {
			return nil, err
		}

		gconf = &GlobalConfig{}
		err = json.Unmarshal(configBytes, gconf)
		if err != nil {
			return nil, err
		}

	}
	gconf.setAllConfig()

	return gconf, nil
}
func (gconf *GlobalConfig) setAllConfig() {
	gconf.setGeneralConfig()
}
func (gconf *GlobalConfig) setGeneralConfig() {
	log.SetLevel(gconf.General.LogLevel)
}
func defaultConfig() *GlobalConfig {
	return &GlobalConfig{
		General: generalConfig{
			LogLevel: log.WarnLevel,
		},
	}
}
func (gconf *GlobalConfig) WriteConfig() error {
	err := os.MkdirAll(configPath, 0755)
	if err != nil {
		return err
	}
	f, err := os.OpenFile(path.Join(configPath, configFile), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	configBytes, err := json.MarshalIndent(gconf, "", "  ")
	if err != nil {
		return err
	}
	_, err = f.Write(configBytes)
	if err != nil {
		return err
	}
	return nil
}
